package com.bibao.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.entity.VoteEntity;
import com.bibao.entity.VoterEntity;
import com.bibao.model.VoteSummary;
import com.bibao.repository.VoteRepository;
import com.bibao.repository.VoterRepository;

@RunWith(SpringRunner.class)
public class VoteServiceMockTest {
	private VoteServiceImpl service;
	@Mock
	private VoterRepository voterRepo;
	@Mock
	private VoteRepository voteRepo;
	
	@Before
	public void setUp() throws Exception {
		service = new VoteServiceImpl();
		service.setVoteReo(voteRepo);
		service.setVoterRepo(voterRepo);
	}

	@Test
	public void testFetchVoteByGender() {
		List<VoterEntity> voterEntities = createVoters();
		List<VoteEntity> voteEntities = createVotes();
		when(voterRepo.findAll()).thenReturn(voterEntities);
		when(voteRepo.findAll()).thenReturn(voteEntities);
		
		List<VoteSummary> summaries = service.fetchVoteByGender();
		summaries.forEach(vs -> {
			switch (vs.getTargetGroup()) {
			case "MALE": 
				assertEquals(3, vs.getNumOfRepublican());
				assertEquals(1, vs.getNumOfDemocrat());
				break;
			case "FEMALE":
				assertEquals(1, vs.getNumOfRepublican());
				assertEquals(2, vs.getNumOfDemocrat());
				break;
			}
		});
	}
	
	@Test
	public void testFetchVoteByState() {
		List<VoterEntity> voterEntities = createVoters();
		List<VoteEntity> voteEntities = createVotes();
		when(voterRepo.findAll()).thenReturn(voterEntities);
		when(voteRepo.findAll()).thenReturn(voteEntities);
		
		List<VoteSummary> summaries = service.fetchVoteByState();
		summaries.forEach(vs -> {
			switch (vs.getTargetGroup()) {
			case "NY": 
				assertEquals(1, vs.getNumOfRepublican());
				assertEquals(2, vs.getNumOfDemocrat());
				break;
			case "NJ":
				assertEquals(0, vs.getNumOfRepublican());
				assertEquals(1, vs.getNumOfDemocrat());
				break;
			case "SC":
				assertEquals(2, vs.getNumOfRepublican());
				assertEquals(0, vs.getNumOfDemocrat());
				break;
			case "MN":
				assertEquals(1, vs.getNumOfRepublican());
				assertEquals(0, vs.getNumOfDemocrat());
				break;
			}
		});
	}

	private List<VoterEntity> createVoters() {
		List<VoterEntity> entities = new ArrayList<>();
		String[] states = {"NY", "NY", "NJ", "SC", "MN", "SC", "NY"};
		int[] ages = {35, 19, 28, 71, 62, 51, 55};
		String[] genders = {"MALE", "FEMALE", "MALE", "MALE", "FEMALE", "MALE", "FEMALE"};
		for (int i=0; i<7; i++) {
			VoterEntity entity = new VoterEntity();
			entity.setId(i+1);
			entity.setAge(ages[i]);
			entity.setGender(genders[i]);
			entity.setState(states[i]);
			entities.add(entity);
		}
		return entities;
	}
	
	private List<VoteEntity> createVotes() {
		List<VoteEntity> entities = new ArrayList<>();
		int[] candidates = {10, 20, 20, 10, 10, 10, 20};
		for (int i=0; i<7; i++) {
			VoteEntity entity = new VoteEntity();
			entity.setVoterId(i+1);
			entity.setCandidateId(candidates[i]);
			entities.add(entity);
		}
		return entities;
	}
}
