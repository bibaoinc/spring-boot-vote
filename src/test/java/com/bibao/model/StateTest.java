package com.bibao.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class StateTest {

	@Test
	public void testGetState() {
		assertEquals(State.NJ, State.getState("nj"));
		assertEquals(State.MD, State.getState("Maryland"));
		assertEquals(State.NY, State.getState("NY"));
		assertEquals(State.OH, State.getState("OHio"));
		assertEquals(State.NM, State.getState("New Mexico"));
		assertEquals(State.UN, State.getState("AB"));
		assertEquals(State.UN, State.getState("Masachusetts"));
	}

}
