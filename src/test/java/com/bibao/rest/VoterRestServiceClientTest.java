package com.bibao.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.bibao.model.Voter;
import com.bibao.model.Gender;
import com.bibao.model.State;
import com.bibao.rest.model.VoterResponse;

public class VoterRestServiceClientTest {
	private static final String BASE_URI = "http://localhost:8080/spring-boot-vote/rest/voter";
	private RestTemplate template;
	
	@Before
	public void setUp() throws Exception {
		template = new RestTemplate();
	}

	@Test
	public void testFetchAllVoters() {
		List<?> voters = template.getForObject(BASE_URI, List.class);
		assertFalse(CollectionUtils.isEmpty(voters));
	}

	@Test
	public void testFetchVotersByGender() {
		String genderURI = BASE_URI + "/search/gender/{gender}";
		ResponseEntity<VoterResponse> response = template.getForEntity(genderURI, VoterResponse.class, "Male");
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<Voter> voters = response.getBody().getVoters();
		voters.forEach(voter -> assertEquals(Gender.MALE, voter.getGender()));
	}
	
	@Test
	public void testFetchVotersByState() {
		String stateURI = BASE_URI + "/search/state/{state}";
		ResponseEntity<VoterResponse> response = template.getForEntity(stateURI, VoterResponse.class, "New Jersey");
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<Voter> voters = response.getBody().getVoters();
		voters.forEach(voter -> assertEquals(State.NJ, voter.getState()));
		ResponseEntity<VoterResponse> response2 = template.getForEntity(stateURI, VoterResponse.class, "Shanghai");
		assertEquals(HttpStatus.OK, response2.getStatusCode());
		assertEquals(HttpStatus.NO_CONTENT, response2.getBody().getStatus());
		assertEquals("No records found", response2.getBody().getMessage());
	}
	
	@Test
	public void testFetchVoterByAgeBetween() {
		String ageURI = BASE_URI + "/search/age/{minAge}/{maxAge}";
		ResponseEntity<VoterResponse> response = template.getForEntity(ageURI, VoterResponse.class, "20", "40");
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<Voter> voters = response.getBody().getVoters();
		voters.forEach(voter -> {
			int age = voter.getAge();
			assertTrue(age>=20 && age<=40);
		});
		ResponseEntity<VoterResponse> response2 = template.getForEntity(ageURI, VoterResponse.class, "5", "15");
		assertEquals(HttpStatus.OK, response2.getStatusCode());
		assertEquals(HttpStatus.NO_CONTENT, response2.getBody().getStatus());
		assertEquals("No records found", response2.getBody().getMessage());
	}
	
	@Test
	public void testRegisterVoter() {
		Voter voter = new Voter();
		voter.setAge(51);
		voter.setGender(Gender.FEMALE);
		voter.setState("New Mexico");
		ResponseEntity<VoterResponse> response = template.postForEntity(BASE_URI, voter, VoterResponse.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Successfully registered a voter", response.getBody().getMessage());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
	}
	
}
