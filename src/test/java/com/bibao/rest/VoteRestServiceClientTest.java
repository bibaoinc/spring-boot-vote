package com.bibao.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.bibao.model.VoteSummary;
import com.bibao.rest.model.VoteResponse;

public class VoteRestServiceClientTest {

	private static final String BASE_URI = "http://localhost:8080/spring-boot-vote/rest/vote";
	private RestTemplate template;
	
	@Before
	public void setUp() throws Exception {
		template = new RestTemplate();
	}

	@Test
	public void testFetchVoteSummaryByGender() {
		String genderURI = BASE_URI + "/gender";
		ResponseEntity<VoteResponse> response = template.getForEntity(genderURI, VoteResponse.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<VoteSummary> summaries = response.getBody().getSummaries();
		assertEquals(2, summaries.size());
		summaries.forEach(summary -> {
			System.out.println(summary);
		});
	}
	
	@Test
	public void testFetchVoteSummaryByState() {
		String stateURI = BASE_URI + "/state";
		ResponseEntity<VoteResponse> response = template.getForEntity(stateURI, VoteResponse.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<VoteSummary> summaries = response.getBody().getSummaries();
		assertEquals(50, summaries.size());
		summaries.forEach(summary -> {
			System.out.println(summary);
		});
	}
}
