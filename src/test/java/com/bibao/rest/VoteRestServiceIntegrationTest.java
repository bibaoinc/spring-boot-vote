package com.bibao.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.model.VoteResult;
import com.bibao.model.VoteSummary;
import com.bibao.rest.model.VoteResponse;
import com.bibao.springbootvote.SpringBootVoteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootVoteApplication.class)
public class VoteRestServiceIntegrationTest {
	@Autowired
	private VoteRestService service;
	
	@Test
	public void testFetchVoteSummaryByGender() {
		ResponseEntity<VoteResponse> response = service.fetchVoteSummaryByGender();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<VoteSummary> summaries = response.getBody().getSummaries();
		assertEquals(2, summaries.size());
		summaries.forEach(summary -> {
			System.out.println(summary);
		});
	}
	
	@Test
	public void testFetchVoteSummaryByState() {
		ResponseEntity<VoteResponse> response = service.fetchVoteSummaryByState();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<VoteSummary> summaries = response.getBody().getSummaries();
		assertEquals(50, summaries.size());
		summaries.forEach(summary -> {
			System.out.println(summary);
		});
	}

	@Test
	public void testFetchVoteResult() {
		ResponseEntity<VoteResult> response = service.fetchVoteResult();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		System.out.println(response.getBody().getMessage());
	}
}
