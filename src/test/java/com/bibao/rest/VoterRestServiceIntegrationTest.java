package com.bibao.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.model.Gender;
import com.bibao.model.State;
import com.bibao.model.Voter;
import com.bibao.rest.model.VoterResponse;
import com.bibao.springbootvote.SpringBootVoteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootVoteApplication.class)
public class VoterRestServiceIntegrationTest {
	@Autowired
	private VoterRestService service;
	
	@Test
	public void testFetchAllVoters() {
		List<Voter> voters = service.fetchAllVoters();
		assertFalse(CollectionUtils.isEmpty(voters));
	}

	@Test
	public void testFetchVotersByGender() {
		ResponseEntity<VoterResponse> response = service.fetchVotersByGender("Female");
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<Voter> voters = response.getBody().getVoters();
		voters.forEach(voter -> assertEquals(Gender.FEMALE, voter.getGender()));
	}
	
	@Test
	public void testFetchVotersByState() {
		ResponseEntity<VoterResponse> response = service.fetchVotersByState("New Jersey");
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<Voter> voters = response.getBody().getVoters();
		voters.forEach(voter -> assertEquals(State.NJ, voter.getState()));
		ResponseEntity<VoterResponse> response2 = service.fetchVotersByState("Beijing");
		assertEquals(HttpStatus.OK, response2.getStatusCode());
		assertEquals(HttpStatus.NO_CONTENT, response2.getBody().getStatus());
		assertEquals("No records found", response2.getBody().getMessage());
	}
	
	@Test
	public void testFetchVoterByAgeBetween() {
		ResponseEntity<VoterResponse> response = service.fetchVotersByAgeBetween(30, 60);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
		assertEquals("Successfully fetch data", response.getBody().getMessage());
		List<Voter> voters = response.getBody().getVoters();
		voters.forEach(voter -> {
			int age = voter.getAge();
			assertTrue(age>=30 && age<=60);
		});
		ResponseEntity<VoterResponse> response2 = service.fetchVotersByAgeBetween(2, 16);
		assertEquals(HttpStatus.OK, response2.getStatusCode());
		assertEquals(HttpStatus.NO_CONTENT, response2.getBody().getStatus());
		assertEquals("No records found", response2.getBody().getMessage());
	}
	
	@Test
	public void testRegisterVoter() {
		Voter voter = new Voter();
		voter.setAge(43);
		voter.setGender(Gender.MALE);
		voter.setState("Virginia");
		ResponseEntity<VoterResponse> response = service.registerVoter(voter);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Successfully registered a voter", response.getBody().getMessage());
		assertEquals(HttpStatus.OK, response.getBody().getStatus());
	}

}
