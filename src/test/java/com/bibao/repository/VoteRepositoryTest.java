package com.bibao.repository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.springbootvote.SpringBootVoteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootVoteApplication.class)
public class VoteRepositoryTest {
	@Autowired
	private VoteRepository repo;
	
	@Test
	public void testFetchVoteResult() {
		List<Object[]> results = repo.fetchVoteResult();
		assertEquals(2, results.size());
		results.forEach(obj -> {
			System.out.println(obj[0] + ": " + obj[1]);
		});
	}

}
