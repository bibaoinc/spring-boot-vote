package com.bibao.repository;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.entity.CandidateEntity;
import com.bibao.springbootvote.SpringBootVoteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootVoteApplication.class)
public class CandidateRepositoryTest {
	@Autowired
	private CandidateRepository repo;
	
	@Test
	public void testFindById() {
		CandidateEntity trump = repo.findById(10).get();
		assertEquals("Donald", trump.getFirstName());
		assertEquals("Trump", trump.getLastName());
		assertEquals("Republican", trump.getParty());
		CandidateEntity clinton = repo.findById(20).get();
		assertEquals("Hillary", clinton.getFirstName());
		assertEquals("Clinton", clinton.getLastName());
		assertEquals("Democrat", clinton.getParty());
	}

}
