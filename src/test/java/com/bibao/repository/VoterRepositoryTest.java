package com.bibao.repository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.entity.VoterEntity;
import com.bibao.model.Gender;
import com.bibao.model.State;
import com.bibao.springbootvote.SpringBootVoteApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootVoteApplication.class)
public class VoterRepositoryTest {
	@Autowired
	private VoterRepository repo;
	
	@Test
	public void testFindByState() {
		List<VoterEntity> entities = repo.findByState("NJ");
		if (!CollectionUtils.isEmpty(entities)) {
			entities.forEach(e -> {
				assertEquals(State.NJ, State.getState(e.getState()));
			});
		}
	}

	@Test
	public void testFindByGender() {
		List<VoterEntity> entities = repo.findByGender(Gender.MALE.toString());
		assertFalse(CollectionUtils.isEmpty(entities));
		entities.forEach(e -> {
			assertEquals(Gender.MALE, Gender.getGender(e.getGender()));
		});
	}
	
	@Test
	public void testFindByAge() {
		List<VoterEntity> entities = repo.findByAge(20, 40);
		if (!CollectionUtils.isEmpty(entities)) {
			entities.forEach(e -> {
				int age = e.getAge();
				assertTrue(age>=20 && age<=40);
			});
		}
	}
}
