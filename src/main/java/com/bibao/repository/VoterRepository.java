package com.bibao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bibao.entity.VoterEntity;

public interface VoterRepository extends JpaRepository<VoterEntity, Integer> {
	public List<VoterEntity> findByState(String state);
	
	public List<VoterEntity> findByGender(String gender);
	
	@Query("select e from VoterEntity e where e.age between :minAge and :maxAge")
	public List<VoterEntity> findByAge(@Param("minAge") int minAge, @Param("maxAge") int maxAge);
}
