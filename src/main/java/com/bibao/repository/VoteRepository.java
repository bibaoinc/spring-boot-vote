package com.bibao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bibao.entity.VoteEntity;

public interface VoteRepository extends JpaRepository<VoteEntity, Integer> {
	@Query("select ve.candidateId, count(ve) from VoteEntity ve group by ve.candidateId")
	public List<Object[]> fetchVoteResult();
}
