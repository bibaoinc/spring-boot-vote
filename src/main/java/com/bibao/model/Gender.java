package com.bibao.model;

public enum Gender {
	MALE, FEMALE;
	
	public static Gender getGender(String input) {
		if (input==null) return null;
		for (Gender gender: Gender.values()) {
			if (gender.toString().equalsIgnoreCase(input)) {
				return gender;
			}
		}
		return null;
	}
}
