package com.bibao.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.bibao.mapper.VoterMapper;
import com.bibao.model.Gender;
import com.bibao.model.State;
import com.bibao.model.Voter;
import com.bibao.repository.VoterRepository;

@Service
public class VoterServiceImpl implements VoterService {
	@Autowired
	private VoterRepository voterRepo;
	
	@Override
	public List<Voter> findVotersWithGender(String gender) {
		return VoterMapper.mapToVoters(voterRepo.findByGender(Gender.getGender(gender).toString()));
	}

	@Override
	public List<Voter> findVoterWithState(String state) {
		return VoterMapper.mapToVoters(voterRepo.findByState(State.getState(state).getAbbreviation()));
	}

	@Override
	public List<Voter> findVoterWithAge(int minAge, int maxAge) {
		return VoterMapper.mapToVoters(voterRepo.findByAge(minAge, maxAge));
	}
	
	@Override
	@Cacheable(value = "voters")
	public List<Voter> findAllVoters() {
		try {
			Thread.sleep(5000);
		} catch (Exception e) {}
		return VoterMapper.mapToVoters(voterRepo.findAll())
				.stream().sorted((v1, v2) -> v1.getId() - v2.getId())
				.collect(Collectors.toList());
	}

	@Override
	@CacheEvict(value = "voters", allEntries = true)
	public void saveVoter(Voter voter) {
		voterRepo.save(VoterMapper.mapToVoterEntity(voter));
	}

	@Override
	@CacheEvict(value = "voters", allEntries = true)
	public void updateVoter(Voter voter) {
		if (voterRepo.findById(voter.getId()).isPresent()) {
			voterRepo.save(VoterMapper.mapToVoterEntity(voter));
		}
	}

}
