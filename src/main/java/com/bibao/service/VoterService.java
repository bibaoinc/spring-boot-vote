package com.bibao.service;

import java.util.List;

import com.bibao.model.Voter;

public interface VoterService {
	public List<Voter> findVotersWithGender(String gender);
	public List<Voter> findVoterWithState(String state);
	public List<Voter> findVoterWithAge(int minAge, int maxAge);
	public List<Voter> findAllVoters();
	public void saveVoter(Voter voter);
	public void updateVoter(Voter voter);
}
