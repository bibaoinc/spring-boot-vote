package com.bibao.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.bibao.entity.CandidateEntity;
import com.bibao.entity.VoteEntity;
import com.bibao.entity.VoterEntity;
import com.bibao.mapper.VoteMapper;
import com.bibao.model.Candidate;
import com.bibao.model.Gender;
import com.bibao.model.State;
import com.bibao.model.Vote;
import com.bibao.model.VoteResult;
import com.bibao.model.VoteSummary;
import com.bibao.repository.CandidateRepository;
import com.bibao.repository.VoteRepository;
import com.bibao.repository.VoterRepository;

@Service
public class VoteServiceImpl implements VoteService {
	@Autowired
	private VoteRepository voteRepo;
	@Autowired
	private VoterRepository voterRepo;
	@Autowired
	private CandidateRepository candidateRepo;
	
	@Override
	public List<VoteSummary> fetchVoteByGender() {
		List<VoterEntity> voterEntities = voterRepo.findAll();
		Map<Integer, Gender> genderMap = voterEntities.stream()
										.collect(Collectors.toMap(VoterEntity::getId, e -> Gender.getGender(e.getGender())));
		List<VoteEntity> voteEntities = voteRepo.findAll();
		Map<Gender, VoteSummary> summaryMap = new HashMap<>();
		for (Gender gender: Gender.values()) {
			VoteSummary vs = new VoteSummary();
			vs.setTargetGroup(gender.toString());
			summaryMap.put(gender, vs);
		}
		voteEntities.forEach(e -> {
			Gender gender = genderMap.get(e.getVoterId());
			switch (e.getCandidateId()) {
				case 10: summaryMap.get(gender).increaseNumOfRepublican(); break;
				case 20: summaryMap.get(gender).increaseNumOfDemocrat(); break;
			}
		});
		return summaryMap.values().stream().collect(Collectors.toList());
	}

	@Override
	public List<VoteSummary> fetchVoteByState() {
		List<VoterEntity> voterEntities = voterRepo.findAll();
		Map<Integer, State> stateMap = voterEntities.stream()
										.collect(Collectors.toMap(VoterEntity::getId, e -> State.getState(e.getState())));
		List<VoteEntity> voteEntities = voteRepo.findAll();
		Map<State, VoteSummary> summaryMap = new HashMap<>();
		for (State state: State.values()) {
			if (state==State.UN) continue;
			VoteSummary vs = new VoteSummary();
			vs.setTargetGroup(state.getAbbreviation());
			summaryMap.put(state, vs);
		}
		voteEntities.forEach(e -> {
			State state = stateMap.get(e.getVoterId());
			switch (e.getCandidateId()) {
				case 10: summaryMap.get(state).increaseNumOfRepublican(); break;
				case 20: summaryMap.get(state).increaseNumOfDemocrat(); break;
			}
		});
		return summaryMap.values().stream()
				.sorted((s1, s2) -> s1.getTargetGroup().compareTo(s2.getTargetGroup()))
				.collect(Collectors.toList());
	}

	@Override
	public void saveVote(Vote vote) {
		voteRepo.save(VoteMapper.mapToVoteEntity(vote));
	}
	
	@Override
	public VoteResult fetchVoteResult() {
		Map<Integer, Candidate> candidateMap = this.fetchCandidateInfo();
		List<Object[]> objList = voteRepo.fetchVoteResult();
		int maxVote = 0;
		int maxVoteId = -1;
		for (Object[] obj: objList) {
			int id = (Integer)obj[0];
			Candidate candidate = candidateMap.get(id);
			candidate.setNumOfVotes(((Long)obj[1]).intValue());
			if (candidate.getNumOfVotes()>maxVote) {
				maxVote = candidate.getNumOfVotes();
				maxVoteId = candidate.getId();
			}
		}
		Candidate winner = candidateMap.get(maxVoteId);
		VoteResult vr = new VoteResult();
		vr.setMessage(winner.getName() + "(" + winner.getParty() + ") wins!");
		vr.setCandidates(candidateMap.values().stream().collect(Collectors.toList()));
		return vr;
	}
	
	@Cacheable(value="candidate")
	private Map<Integer, Candidate> fetchCandidateInfo() {
		List<CandidateEntity> entities = candidateRepo.findAll();
		Map<Integer, Candidate> candidateMap = new HashMap<>();
		entities.forEach(e -> {
			Candidate candidate = new Candidate();
			candidate.setId(e.getId());
			candidate.setName(e.getFirstName() + " " + e.getLastName());
			candidate.setParty(e.getParty());
			candidateMap.put(e.getId(), candidate);
		});
		return candidateMap;
	}
	
	public void setVoteReo(VoteRepository voteRepo) {
		this.voteRepo = voteRepo;
	}
	
	public void setVoterRepo(VoterRepository voterRepo) {
		this.voterRepo = voterRepo;
	}
}
