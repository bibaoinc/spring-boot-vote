package com.bibao.springbootvote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages="com.bibao")
@EnableJpaRepositories(basePackages = {"com.bibao.repository"})
@EntityScan(basePackages = {"com.bibao.entity"})
@EnableCaching
public class SpringBootVoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootVoteApplication.class, args);
	}
}
