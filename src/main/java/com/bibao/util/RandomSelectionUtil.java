package com.bibao.util;

import java.util.List;
import java.util.Random;

import com.bibao.model.Gender;
import com.bibao.model.State;

public class RandomSelectionUtil {
	private static final Random random = new Random();
	
	// Random select an age between 18 and 90
	public static int selectAge() {
		return random.nextInt(73) + 18;
	}
	
	public static State selectState() {
		State[] states = State.values();
		int selectedIndex = random.nextInt(states.length-1);
		return states[selectedIndex];
	}
	
	public static Gender selectGender() {
		int n = random.nextInt(100);
		if (n%2==0) return Gender.MALE;
		else return Gender.FEMALE;
	}
	
	public static int selectCandidate(List<Integer> candidateIds) {
		int n = candidateIds.size();
		int selectedIndex = random.nextInt(n);
		return candidateIds.get(selectedIndex);
	}
}
