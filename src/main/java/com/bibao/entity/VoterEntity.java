package com.bibao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="voter")
public class VoterEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "voter_id_generator")
	@SequenceGenerator(name="voter_id_generator", sequenceName = "voter_seq", allocationSize=1)
    @Column(name="voter_id")
	private int id;
	
	@Column
	private int age;
	
	@Column
	private String state;
	
	@Column
	private String gender;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
}
