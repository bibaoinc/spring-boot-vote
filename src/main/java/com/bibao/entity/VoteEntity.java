package com.bibao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="vote")
public class VoteEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vote_id_generator")
	@SequenceGenerator(name="vote_id_generator", sequenceName = "vote_seq", allocationSize=1)
    @Column(name="vote_id")
	private int id;
	
	@Column(name="voter_id")
	private int voterId;
	
	@Column(name="candidate_id")
	private int candidateId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVoterId() {
		return voterId;
	}

	public void setVoterId(int voterId) {
		this.voterId = voterId;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}
	
}
