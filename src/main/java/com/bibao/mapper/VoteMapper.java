package com.bibao.mapper;

import com.bibao.entity.VoteEntity;
import com.bibao.model.Vote;

public class VoteMapper {
	public static VoteEntity mapToVoteEntity(Vote vote) {
		VoteEntity entity = new VoteEntity();
		entity.setId(vote.getId());
		entity.setVoterId(vote.getVoterId());
		entity.setCandidateId(vote.getCandidateId());
		return entity;
	}
}
