package com.bibao.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.bibao.entity.VoterEntity;
import com.bibao.model.Voter;

public class VoterMapper {
	public static VoterEntity mapToVoterEntity(Voter voter) {
		VoterEntity entity = new VoterEntity();
		entity.setId(voter.getId());
		entity.setAge(voter.getAge());
		entity.setGender(voter.getGender().toString());
		entity.setState(voter.getState().getAbbreviation());
		return entity;
	}
	
	public static Voter mapToVoter(VoterEntity entity) {
		Voter voter = new Voter();
		voter.setId(entity.getId());
		voter.setGender(entity.getGender());
		voter.setAge(entity.getAge());
		voter.setState(entity.getState());
		return voter;
	}
	
	public static List<Voter> mapToVoters(List<VoterEntity> entities) {
		if (!CollectionUtils.isEmpty(entities)) {
			return entities.stream().map(e -> VoterMapper.mapToVoter(e)).collect(Collectors.toList());
		} 
		return new ArrayList<>();
	}
}
