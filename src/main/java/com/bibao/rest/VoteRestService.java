package com.bibao.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.model.Vote;
import com.bibao.model.VoteResult;
import com.bibao.model.VoteSummary;
import com.bibao.rest.mapper.ResponseMapper;
import com.bibao.rest.model.VoteResponse;
import com.bibao.service.VoteService;

@RestController
@RequestMapping("/vote")
public class VoteRestService {
	@Autowired
	private VoteService voteService;
	
	@GetMapping("/gender")
	public ResponseEntity<VoteResponse> fetchVoteSummaryByGender() {
		List<VoteSummary> summaries = voteService.fetchVoteByGender();
		return ResponseMapper.mapToVoteResponse(summaries);
	}
	
	@GetMapping("/state")
	public ResponseEntity<VoteResponse> fetchVoteSummaryByState() {
		List<VoteSummary> summaries = voteService.fetchVoteByState();
		return ResponseMapper.mapToVoteResponse(summaries);
	}
	
	@GetMapping
	public ResponseEntity<VoteResult> fetchVoteResult() {
		return new ResponseEntity<VoteResult>(voteService.fetchVoteResult(), HttpStatus.OK);
	}
	
	@PostMapping
	public void takeVote(@RequestBody Vote vote) {
		voteService.saveVote(vote);
	}
}
