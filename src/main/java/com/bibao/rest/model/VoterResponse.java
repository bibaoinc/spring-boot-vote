package com.bibao.rest.model;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.bibao.model.Voter;

public class VoterResponse {
	private Voter voter;
	private List<Voter> voters;
	private String message;
	private HttpStatus status;
	
	public Voter getVoter() {
		return voter;
	}
	public void setVoter(Voter voter) {
		this.voter = voter;
	}
	public List<Voter> getVoters() {
		return voters;
	}
	public void setVoters(List<Voter> voters) {
		this.voters = voters;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
