package com.bibao.rest.model;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.bibao.model.VoteSummary;

public class VoteResponse {
	private List<VoteSummary> summaries;
	private String message;
	private HttpStatus statusCode;
	
	public List<VoteSummary> getSummaries() {
		return summaries;
	}
	public void setSummaries(List<VoteSummary> summaries) {
		this.summaries = summaries;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
}
