package com.bibao.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.model.Voter;
import com.bibao.rest.mapper.ResponseMapper;
import com.bibao.rest.model.VoterResponse;
import com.bibao.service.VoterService;

@RestController
@RequestMapping("/voter")
public class VoterRestService {
	@Autowired
	private VoterService voterService;
	
	@GetMapping("/ping")
	public String ping() {
		return "Ping Voter Rest Service";
	}
	
	@GetMapping
	public List<Voter> fetchAllVoters() {
		return voterService.findAllVoters();
	}
	
	@GetMapping("/search/state/{state}")
	public ResponseEntity<VoterResponse> fetchVotersByState(@PathVariable("state") String state) {
		List<Voter> voters = voterService.findVoterWithState(state);
		return ResponseMapper.mapToVoterResponse(voters);
	}
	
	@GetMapping("/search/gender/{gender}")
	public ResponseEntity<VoterResponse> fetchVotersByGender(@PathVariable("gender") String gender) {
		List<Voter> voters = voterService.findVotersWithGender(gender);
		return ResponseMapper.mapToVoterResponse(voters);
	}
	
	@GetMapping("/search/minage/{minAge}")
	public ResponseEntity<VoterResponse> fetchVotersByMinAge(@PathVariable("minAge") int minAge) {
		List<Voter> voters = voterService.findVoterWithAge(minAge, Integer.MAX_VALUE);
		return ResponseMapper.mapToVoterResponse(voters);
	}
	
	@GetMapping("/search/maxage/{maxAge}")
	public ResponseEntity<VoterResponse> fetchVotersByMaxAge(@PathVariable("maxAge") int maxAge) {
		List<Voter> voters = voterService.findVoterWithAge(18, maxAge);
		return ResponseMapper.mapToVoterResponse(voters);
	}
	
	@GetMapping("/search/age/{minAge}/{maxAge}")
	public ResponseEntity<VoterResponse> fetchVotersByAgeBetween(@PathVariable("minAge") int minAge, @PathVariable("maxAge") int maxAge) {
		List<Voter> voters = voterService.findVoterWithAge(minAge, maxAge);
		return ResponseMapper.mapToVoterResponse(voters);
	}
	
	@PostMapping
	public ResponseEntity<VoterResponse> registerVoter(@RequestBody Voter voter) {
		try {
			voterService.saveVoter(voter);
			return ResponseMapper.mapToVoterResponse(voter, HttpStatus.OK, "Successfully registered a voter");
		} catch (Exception e) {
			return ResponseMapper.mapToVoterResponse(voter, HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	@PutMapping
	public void updateVoter(@RequestBody Voter voter) {
		voterService.updateVoter(voter);
	}
}
